
package my.android.namespace;
import java.io.UnsupportedEncodingException;
import org.bouncycastle.crypto.BlockCipher;
import org.bouncycastle.crypto.BufferedBlockCipher;
import org.bouncycastle.crypto.CryptoException;
import org.bouncycastle.crypto.engines.BlowfishEngine;
import org.bouncycastle.crypto.engines.DESEngine;
import org.bouncycastle.crypto.engines.RijndaelEngine;
import org.bouncycastle.crypto.engines.SerpentEngine;
import org.bouncycastle.crypto.engines.TwofishEngine;
import org.bouncycastle.crypto.modes.CBCBlockCipher;
import org.bouncycastle.crypto.modes.PaddedBlockCipher;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.util.encoders.Hex;
/**
 *
 * Klasa odpowiedzialna za Kryptografie całego systemu.
 * @author Daniel Marczydło
 */

public class Encryptor { 
  
    /*
     * 
     */
    private BufferedBlockCipher cipher; 
  
    private KeyParameter key; 

    /**
     * zmienna statyczna określająca numer dla algorytmu DES
     */
    public static final byte TYPE_DES = 2; 

    /**
    * zmienna statyczna określająca numer dla algorytmu AES
     */
    public static final byte TYPE_AES = 1; 

    /**
     * zmienna statyczna określająca numer dla algorytmu TwoFish
     */
    public static final byte TYPE_TWOFISH = 3; 

    /**
     * zmienna statyczna określająca numer dla algorytmu BlowFish
     */
    public static final byte TYPE_BLOWFISH = 4; 

    
   

    // Initialize the cryptographic engine. 
    // The key array should be at least 8 bytes long. 

    
 

    
    /**
     * Konstruktor klasy, pobiera klucz oraz rodzaj algorytmu.
     * @param key
     * @param cipherType
     */
    public Encryptor (byte[] key, byte cipherType) { 

        BlockCipher theCipher = null; 
        switch (cipherType) { 
        case TYPE_DES: 
            theCipher = new DESEngine(); 
            break; 
        case TYPE_AES: 
            theCipher = new RijndaelEngine(); 
            break; 
        case TYPE_TWOFISH: 
            theCipher = new TwofishEngine(); 
            break; 
        case TYPE_BLOWFISH: 
            theCipher = new BlowfishEngine(); 
            break; 
        
        } 
        ; 

        cipher = new PaddedBlockCipher(new CBCBlockCipher(theCipher)); 
        // cipher = new CTSBlockCipher(new CBCBlockCipher (theCipher)) ; 

        this.key = new KeyParameter(key); 
    } 

    // Initialize the cryptographic engine. 
    // The string should be at least 8 chars long. 

    /**
     * Konstruktor klasy 
     * @param key
     * @param cipherType
     */
    public Encryptor (String key, byte cipherType) { 
        this(key.getBytes(), cipherType); 
    } 

    // Private routine that does the gritty work. 

    private byte[] callCipher(byte[] data) throws CryptoException { 
        int size = cipher.getOutputSize(data.length); 
        byte[] result = new byte[size]; 
        int olen = cipher.processBytes(data, 0, data.length, result, 0); 
        olen += cipher.doFinal(result, olen); 

        if (olen < size) { 
            byte[] tmp = new byte[olen]; 
            System.arraycopy(result, 0, tmp, 0, olen); 
            result = tmp; 
        } 

        return result; 
    } 

    // Encrypt arbitrary byte array, returning the 
    // encrypted data in a different byte array. 

    /**
     * Klasa której zadaniem jest zaszyfrowanie tablicy bajtów
     * @param data
     * @return
     * @throws CryptoException
     */
    public synchronized byte[] encrypt(byte[] data) throws CryptoException { 
        if (data == null || data.length == 0) { 
            return new byte[0]; 
        } 

        cipher.init(true, key); 
        return callCipher(data); 
    } 

    /**
     * Klasa, której zadaniem jest zaszyfrowanie danych. 
     * @param data
     * @return
     * @throws CryptoException
     */
    public synchronized String encrypt(String data) throws CryptoException { 
        if (data == null || data.length() == 0) { 
            return null; 
        } 

        String response = null; 
        try { 
            response = new 
String(Hex.encode(encrypt(data.getBytes("UTF-8")))); 
        } catch (Exception e) { 
            response = new String(Hex.encode(encrypt(data.getBytes()))); 
        } 

        return response; 
    } 

    /**
     * Klasa której zadaniem jest odszyfrowanie tablicy bajtów.
     * @param data 
     * @return
     * @throws CryptoException
     */
    public synchronized byte[] decrypt(byte[] data) throws CryptoException { 
        if (data == null || data.length == 0) { 
            return new byte[0]; 
        } 

        cipher.init(false, key); 
        return callCipher(data); 
    } 

    /**
     * Klasa której zadaniej jest odszyfrowanie ciągu znaków.
     * @param data
     * @return
     * @throws CryptoException
     */
    public synchronized String decrypt(String data) throws CryptoException { 
        if (data == null || data.length() == 0) { 
            return null; 
        } 
        String result = null; 
                try{ 
                     result = new String (decrypt (Hex.decode (data)),"UTF-8"); 
                }catch(Exception e){ 
                    result = new String (decrypt (Hex.decode (data))); 
                    e.printStackTrace (); 
                } 
        return result; 
    } 

} 
