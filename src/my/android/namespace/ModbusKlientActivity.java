package my.android.namespace;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

/**
 * Klasa odpowiedzialna za obsługę zdarzeń ekranu startowego "Splash"
 * @author Daniel Marczydło
 */
public class ModbusKlientActivity extends Activity {
    /** Called when the activity is first created.
     * @param savedInstanceState 
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        Thread startSplash = new Thread(){
        	public void run()
        	{
        		try
        		{
        			int timer = 0;
        			while(timer < 2000)
        			{
        				sleep(100);
        				timer+=100;
        				
        			}
        			startActivity(new Intent("my.android.namespace.MAIN"));
        		} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        		
        		finally
        		{
        			finish();
        		}
        	}	
        };
        
        startSplash.start();
    }
}