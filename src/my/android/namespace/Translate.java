package my.android.namespace;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;

/**
 * Klasa odpowiedzialna za przesunięcia bitowe dla strumienia. Pomocna podczas wysyłania oraz odbierania danych od serwera.
 * @author Daniel Marczydło
 */
public class Translate {
    	
    /**
     * Metoda przygotowująca dane do wysłania.
     * @param os
     * @param i
     * @throws IOException
     */
    public  void Write7BitEncodedInt(OutputStream os, int i) throws IOException 
    	{ 
    	        while(i < 0 || i > 127) 
    	        { 
    	                byte y = (byte) (0x80 | i & 127); 
    	                os.write(y); 
    	                i = i >>> 7; 
    	        } 

    	        os.write(i); 
    	} 
    	
        /**
         * Metoda przygotowująca dane do odczytu.
         * @param is
         * @return
         * @throws IOException
         */
        public  int Read7BitEncodedInt(InputStream is) throws IOException 
    	{ 
    	        int n = 0; 
    	        int x = 0x80; 
    	        int shift = 0; 

    	        while((x & 0x80) != 0) 
    	        { 
    	                x = is.read(); 
    	                n = n | ((x & 0x7F) << shift); 
    	                shift = shift + 7; 
    	        } 

    	        return n; 
    	        
    	}
    	
        /**
         * Metoda czytająca dane w poprawny sposób.
         * @param is
         * @return
         * @throws IOException
         */
        public  String ReadCSharpString(InputStream is) throws IOException 
    	{ 
    	        int c = Read7BitEncodedInt(is); 
    	        byte[] buf = new byte[c]; 
    	        is.read(buf); 
    	        return new String(buf, "utf-8"); 
    	}
    	

        /**
         * Metoda zapisująca pobrane dane w poprawny sposób.
         * @param os
         * @param s
         * @throws IOException
         */
        public  void WriteCSharpString(OutputStream os, String s) throws IOException 
    	{ 
    	        ByteBuffer utf8Buf = Charset.forName("utf-8").encode(s); 
    	        Write7BitEncodedInt(os, utf8Buf.limit()); 
    	        os.write(utf8Buf.array(), 0, utf8Buf.limit()); 
    	} 
    	

    }
	
	

