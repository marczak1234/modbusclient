package my.android.namespace;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import android.widget.Toast;
import java.io.UnsupportedEncodingException;

/**
 * Klasa odpowiedzialna za obsługę zdarzeń widoku z listą zmiennych. W niej również obsługiwane jest nasłuchiwanie.
 * @author Daniel Marczydło
 */
public class Variable extends ListActivity {
//public String key = "tojestkluczwlasnie";
    /**
     * Lista przechowująca obecną listę zmiennych.
     */
    public List<item> lista =  new ArrayList<item>();
	int  Index;
        /**
         * ciąg znaków, na podstawie którego wygenerowany zostanie klucz.
         */
        public String key = "tojestkluczwlasnie";
        //public byte[] kluczByte =  {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
	Translate t = new Translate();
	private  DatagramSocket udp = null;
        /**
         * 
         */
        public  Encryptor enc;
        /**
         * Metoda zwracająca bajt określający typ szyfrowania.
         * @return
         */
        public static byte getCode()
        {
            byte code = 0;
            String szyfr = main.preferences.getString("SZYFR_TCP", "");

            if(szyfr.equals("2")) code = 1;
            else if(szyfr.equals("5")) code = 2;
            else if(szyfr.equals("10")) code = 3;
            else if(szyfr.equals("60")) code = 4;
            return code;
        }     
        
        /**
         * Metoda zwracająca klucz kryptograficzny w postaci tablicy bajtów.
         * @param key
         * @return
         * @throws UnsupportedEncodingException
         */
        public static byte[] getKeyBytes(String key) throws UnsupportedEncodingException{
        byte[] keyBytes= new byte[16];
        byte[] parameterKeyBytes= key.getBytes("UTF-8");
        System.arraycopy(parameterKeyBytes, 0, keyBytes, 0, Math.min(parameterKeyBytes.length, keyBytes.length));
      return keyBytes;
    }

         // DesEncrypter ds = new DesEncrypter();
   /**
    * Metoda wywoływana na zdarzenie stworzenia opcji menu.
    * @param menu
    * @return
    */
   public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_1,menu);
		return true;
		}
        
   /**
    * Metoda obsługująca wybranie elementu z menu
    * @param item
    * @return
    */
   @Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.zmienne:
                {
                    String s = main.MyAddress+":SEND:";
                   
		
                try {
                                 String szyfr = main.preferences.getString("SZYFR_TCP", "");
                               if(!szyfr.equals("1"))
                               {
                                   Encryptor enc = new Encryptor(getKeyBytes(key), main.getCode());
                                   //Encryptor enc = new Encryptor(kluczByte, getCode());
                                   s = enc.encrypt(s);
                               }
                               
			t.WriteCSharpString(main.socket.getOutputStream(),s);
                       
		} catch (Exception e) {
			
			showContext(e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
                    
                }break;
                    
                    
                    
		}
		// TODO Auto-generated method stub
		return super.onOptionsItemSelected(item);
				
	}
   /**
    * 
    */
   public void udpClose()
	{
		try
		{
			this.udp.close();
		}catch (Exception e) {
			showContext(e.getMessage());
		}
	}
	
        /**
         * Metoda wywyływana podczas tworzenia widoku. 
         * @param savedInstanceState
         */
        @Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		try
		{

			WatekAsk w = new WatekAsk();
		
				w.execute();
			
				
		}catch (Exception e) {
			showContext("Wyjątek od Async = "+e.getMessage());
		}
		String s = main.MyAddress+":SEND:";
                
		
        try {
                 String szyfr = main.preferences.getString("SZYFR_TCP", "");
                               if(!szyfr.equals("1"))
                               {
                                    enc = new Encryptor(getKeyBytes(key), main.getCode());
                                   //Encryptor enc = new Encryptor(kluczByte, getCode());
                                   s = enc.encrypt(s);
                               }       
			t.WriteCSharpString(main.socket.getOutputStream(),s);
                       
		} catch (Exception e) {
			
			showContext(e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
                ListView list = getListView();
		list.setTextFilterEnabled(true);
		list.setOnItemClickListener(new OnItemClickListener(){

			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				try
				{
				   item tmp = new item();
				   tmp = lista.get(arg2);
				
							//showContext(Integer.toString(arg2));
				
				Intent i = new Intent("my.android.namespace.VariableOne");
				i.putExtra("MyClass", tmp);
				
				startActivity(i);
				}catch (Exception e) {
					showContext(e.getMessage());
					// TODO: handle exception
				}
				
				// TODO Auto-generated method stub
				//showContext(((TextView)arg1).getText().toString());
			}
			
		});
	
	}
	
        /**
         * Metoda wywoływana podczas zmiany widoku.
         */
        @Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	
	}
	
	
        /**
         * Metoda wywoływana podczas powrotu do obecnego widoku.
         */
        @Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Napewno chcesz się wylogować?")
		       .setCancelable(false)
		       .setPositiveButton("Tak", new DialogInterface.OnClickListener() {
		           public void onClick(DialogInterface dialog, int id) {
		                finish();
		              
		                try
		                {
                                    
		                	udp.close();
		                	udp = null;
                                        
                                         String s = main.MyAddress+":BYE:pusty:";
                                           String szyfr = main.preferences.getString("SZYFR_TCP", "");
                               if(!szyfr.equals("1"))
                               {
                                  // Encryptor enc = new Encryptor(getKeyBytes(key), main.getCode());
                                   s = enc.encrypt(s);
                               }
                                      t.WriteCSharpString(main.socket.getOutputStream(),s);
                                        
		                }catch (Exception e) {
							// TODO: handle exception
						}
		              //super.onBackPressed();
		           }
		       })
		       .setNegativeButton("Nie", new DialogInterface.OnClickListener() {
		           public void onClick(DialogInterface dialog, int id) {
		                dialog.cancel();
		           }
		       });
		AlertDialog alert = builder.create();
		alert.show();
		
		
	}

        /**
         * Metoda ustawiająca zmienne. Pobiera ciąg znaków dostarczony od serwera, oraz zamienia go na odpowiednie wpisy do listy.
         * @param message
         */
        public void SetVariable(String message)
	{
		int i=0;
		
	    String[] lines = message.split(";");
		
		String tmp_tab[] = new String[lines.length];
		 
		try
		{
			int wielkosc =lines.length; 
		
		 for(int ina=0;ina<wielkosc;ina++)
		 {
			 tmp_tab[ina] = lines[ina];
			 
		 }
		
		
		}catch(Exception ex){this.showContext("Wyjątek " + ex.getLocalizedMessage());}
		
		 String [] tmpNames= new String[tmp_tab.length]; //nazwy zmiennych dla spinera
		 
		 for(String line : tmp_tab)
         {
			 try
			 {
				 
             if (line.equals("brak zmiennych") || line.equals(null))
             {
                 break;
             }
             
             item Item = new item();
             String[] items = line.split(",");
             Item.setNr(i);
             Item.setName(items[0]);
             Item.setAddress(items[1]);
             Item.setType(items[2]);
             Item.setDescription(items[3]);
             tmpNames[i] = items[0];
             
             
             lista.add(i,Item);           	
             i++;

			 }
			 catch(Exception ex){this.showContext("Wyjatek + "+ ex.getLocalizedMessage());}
         }
		 
		 setListAdapter(new ArrayAdapter<String>(this, R.layout.single_item,tmpNames));
	
	}
	
        /**
         * Metoda wyświetlania kontekstowych wiadomości.
         * @param message
         */
        public void showContext(String message)
	{
		CharSequence text = (CharSequence)message;
        Context context = getApplicationContext();
        int duration = Toast.LENGTH_SHORT;
         
        Toast toast = Toast.makeText(context,text,duration);
        toast.show();    
	}
	
        /**
         * Klasa odpowiedzialna za tworzenie wątku związanego z odbieraniem informacji od serwera. Wykorzystuje AsyncAsk
         */
	private class WatekAsk extends AsyncTask<Void, String, Void> 
    {
		
   	    
   	 protected Void doInBackground(Void... params) 
   	 {
   		 try
    		{
   			 
   			 udp = new DatagramSocket(2500);
    			
    			 boolean flagaBYE = true;
    			 
    			 String w = "";
    			while(flagaBYE)
    			{
                           	byte[] receiveData= new byte[1024];
    				
                                DatagramPacket pakiet = new DatagramPacket(receiveData, receiveData.length);
                                udp.receive(pakiet);
    				
    				String wiadomosc = new String(pakiet.getData());
                                wiadomosc = wiadomosc.trim();
                               
                               String szyfr = main.preferences.getString("SZYFR_TCP", "");
                               if(!szyfr.equals("1"))
                               {
                                    Encryptor enc = new Encryptor(getKeyBytes(key), main.getCode());
                                   // enc = new Encryptor(kluczByte, getCode());

                                   wiadomosc = enc.decrypt(wiadomosc);
                                 
                               }
                               
                                //Log.e("wiadomosc = ",wiadomosc);
                                String[] cmd = wiadomosc.split(":");
                                // Log.e("podzielona = ", ":" + cmd[1] + ":");
    				if (cmd[1].equals("BYE"))
    				{
    					udp.close();
    					publishProgress("Rozłączony");
    					flagaBYE = false;
    					main.running = false;
    					main.socketClose();
                                        startActivity(new Intent("my.android.namespace.MAIN"));
    					
    				}
    				
    				if (cmd.length > 2)
    	              {
    					
    					
    					w = cmd[2];
    					for (int i = 3; i < cmd.length; i++)
                        {
                            w += cmd[i];
                        }
    				//Log.e("wartosc ",cmd[1]);	
    					if (cmd[1].equals("SEND"))
                        {
    						 if (w.equals("brak zmiennych"))
    						 {
    							publishProgress("W tej chwili nie ma dostępu do zmiennych");
    							
    						 }
    						 else 
    	                     {
    							publishProgress("Otrzymałem zmienne");
    							//publishProgress(w);
    							publishProgress("*" + w);
    	                     }    						
                        }
    					  else if (cmd[1].equals("SUCCESS"))
    	                    {
    						
    	                       publishProgress("Wartość zmiennej została zmieniona");
    	                    }
    	                    else if (cmd[1].equals("FAILED"))
    	                    {
    	                    	
    	                       publishProgress("Błąd, zmienna nie może zostać zmieniona");
    	                    }
                            
    	                    else if (cmd[1].equals("GET"))
    	                    {
                                //Log.e("get", "jestem");
    	                    	publishProgress("#"+w);
    	                    	
    	                       
    	                    }
    	                    else if(cmd[1].equals("SAY"))
    	                    {
    	                    	publishProgress(w);
    	                    	
    	                    }
    					
    					
    	              }
    				
    		
    			}	
    			
    		}
    		catch(Exception ex)
    		{
    			//publishProgress("Tu : " + ex.getMessage());
    		}
   		 return null;
    	}
   	 
   	 protected void onProgressUpdate(String... values) 
   	 {
   		    super.onProgressUpdate(values);
   		    
   		    if(values[0].charAt(0)=='*')
   		    {
   		    	
   		    	values[0] = values[0].substring(1,values[0].length());
   		    	SetVariable(values[0]);
   		      
   		    }
   		    else if(values[0].charAt(0)=='#')
   		    {
   		    	values[0] = values[0].substring(1,values[0].length());
   		    	
   		    		
   		    		try
   		    		{
   		    			values[0] = values[0].trim();
   		    			if(values[0].length()>0) VariableOne.txtValues.setText(values[0]); else showContext("Brak dostępu do zmiennej");
   		    			//showContext(Integer.toString(values[0].length())+ "= tekstu az tyle: " + values[0]);
   		    			//VariableOne.txtValues.setText(values[0]);
   		    		}catch (Exception e) {
   		    			showContext(e.getMessage());
						// TODO: handle exception
					}
   		    		//dVariableOne.
   		     
   		    	//txtWartosc.setText(values[0]);
   		    }
   		    else if(values[0].equals("Rozłączony"))
   		    {
   		    	finish();
   		    	//startActivity(new Intent("my.android.namespace.MAIN"));
   		    }
   		    
   		    else
   		    {
   		    	showContext(values[0]);
   		        //showContext(values[0]);
   		    }
   		    
   		}
   	 
   	 protected void onPostExecute(Void result) 
   	 {
   		    super.onPostExecute(result);
   		    
   		    
   		    
   	 }
   	 
   	 
    }

	
	
}
