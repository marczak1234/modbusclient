package my.android.namespace;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import java.io.UnsupportedEncodingException;
/**
 * 
 * @author Daniel Marczydło
 */
public class VariableOne extends Activity {
	
	private Button btnSet;
	private Button btnShow;
	private EditText etxtValue;
	private TextView txtName;
	private TextView txtAddress;
	private TextView txtType;
	private TextView txtDesc;
        /**
         * 
         */
        public static TextView txtValues;
	
	private SharedPreferences preferences;
	private Timer timer=null;
	item i = new item();
	
	Translate t = new Translate();
	String nazwazmiennej; 
        /**
         * 
         */
        public String key = "tojestkluczwlasnie";
        /**
         * Metoda odpowiedzialna za wyświetlanie wiadomości kontekstowych.
         * @param message
         */
        public void showContext(String message)
	{
		CharSequence text = (CharSequence)message;
        Context context = getApplicationContext();
        int duration = Toast.LENGTH_SHORT;
         
        Toast toast = Toast.makeText(context,text,duration);
        toast.show();    
	}
	
        /**
         * Metoda odowiedzialna za wygenerowanie klucza kryptograficznego w postaci tablicy bajtów.
         * @param key
         * @return
         * @throws UnsupportedEncodingException
         */
        public static byte[] getKeyBytes(String key) throws UnsupportedEncodingException{
        byte[] keyBytes= new byte[16];
        byte[] parameterKeyBytes= key.getBytes("UTF-8");
        System.arraycopy(parameterKeyBytes, 0, keyBytes, 0, Math.min(parameterKeyBytes.length, keyBytes.length));
      return keyBytes;
    }
   /**
    * Metoda odpowiedzialna za cykliczne generowanie danego zdarzenia.
    * @param czas
    */
   public void startCykl(int czas) {
	    timer = new Timer();
	    timer.schedule( new TimerTask(){
	        public void run(){
	           TimerMethod(); 
	        }
	    },0,  czas*1000 ); // 10 s. 
	}
	
	
   /**
    * 
    */
   public void pause(){
	    timer.cancel();
	}
	
	
        /**
         * Metoda wywoływana podczas powrotu do danego widoku.
         */
        @Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		if(timer!=null)
		{
			pause();
		}
		
		if(preferences.getBoolean("CK_SETTINGS", false))
		{
		
			int czas = Integer.parseInt(preferences.getString("CK_VALUE_SETTINGS", ""));
			
			startCykl(czas);
		}
		
		
		
	}




        /**
         * 
         */
        @Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		try
		{
			if(timer!= null)pause();
		}catch (Exception e) {
			// TODO: handle exception
		}
		
		
	}

        /**
         * Metoda wywoływana podczas tworzenie menu.
         * @param menu
         * @return
         */
        public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu,menu);
		return true;
		}
	

        /**
         * Metoda wywoływana podczas wyboru danego elementu z menu.
         * @param item
         * @return
         */
        @Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.pomoc:
			break;
		case R.id.ustawienia: 
			{
				Intent settingsActivity = new Intent(getBaseContext(),
                        settings.class);
				
				settingsActivity.putExtra("IPPORT", true);
                startActivity(settingsActivity);
				
			}break;
		case R.id.opogramie: 
		{
			PackageInfo pInfo=null;
			try {
				pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			} catch (NameNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		
			
			String version = pInfo.versionName;
			String textT = "Autor : Marczak \nWersja programu : "+ version + "\nOpis : Mobilny klient, którego zadaniem jest zmiana zmiennych procesowych oraz odczytywanie ich wartości (także cyklicznie). \nMożliwości programu : \n- odczyt wartości zmiennych procesowych MODBUS \n- zmiana wartości zmiennych procesowych \n- dostęp do zmiennych sieciowych oraz z poza sieci (VPN).  " ;
			final Dialog dialog = new Dialog(this);
			dialog.setContentView(R.layout.dualogabout);
			dialog.setTitle("Informacje o programie");
			dialog.setCancelable(true);
			TextView text = (TextView) dialog.findViewById(R.id.TextView01);
			text.setText(textT);
			
			Button button = (Button) dialog.findViewById(R.id.Button01);
			
			  dialog.show();
			
            button.setOnClickListener(new OnClickListener() {
           
                public void onClick(View v) {
                  
                	dialog.cancel();
                }
            });
			
          
			 
		
		}break;
		
		//default:	
		
		
		
		
		}
		// TODO Auto-generated method stub
		return super.onOptionsItemSelected(item);
				
	}

	
	
        /**
         * Metoda odpowiedzialna za zdarzenie utworzenia widoku.
         * @param savedInstanceState
         */
        @Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.variable);
		
		preferences = getSharedPreferences("myPreferences", Activity.MODE_PRIVATE);
		boolean cykl = false;
		int okres =0;
		
		
		try
		{

			Intent intent = getIntent();
		
			i = (item)intent.getSerializableExtra("MyClass");
			btnSet = (Button)findViewById(R.id.btnSet);
			btnShow = (Button)findViewById(R.id.btnShow);
			etxtValue = (EditText)findViewById(R.id.etxtValue);
			txtAddress = (TextView)findViewById(R.id.txtAddress);
			txtDesc = (TextView)findViewById(R.id.txtDesc);
			txtName = (TextView)findViewById(R.id.txtName);
			txtType = (TextView)findViewById(R.id.txtType);
			txtValues = (TextView)findViewById(R.id.txtValue);
			
			txtName.setText(i.getName());
			txtAddress.setText(i.getAddress());
			txtType.setText(i.getType());
			txtDesc.setText(i.getDescription());
			
			Translate t = new Translate();
			String nazwazmiennej = i.getName();
			String s = main.MyAddress+":GET:"+nazwazmiennej;
			
			this.nazwazmiennej= i.getName();
			try {
                            
                              String szyfr = main.preferences.getString("SZYFR_TCP", "");
                               if(!szyfr.equals("1"))
                               {
                                   Encryptor enc = new Encryptor(getKeyBytes(key), main.getCode());
                                   //Encryptor enc = new Encryptor(kluczByte, getCode());
                                   s = enc.encrypt(s);
                               }
                            
				t.WriteCSharpString(main.socket.getOutputStream(),s);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}	 //wysy�anie pro�by o zmienna

		}catch (Exception e) {
			showContext(e.getMessage());
			// TODO: handle exception
		}
		
			
		
			btnShow.setOnClickListener(new OnClickListener() {
				
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Translate t = new Translate();
					try
					{
					String nazwazmiennej = i.getName();
                                          
        			String s = main.MyAddress+":GET:"+nazwazmiennej;
                              
                                
                                 String szyfr = main.preferences.getString("SZYFR_TCP", "");
                               if(!szyfr.equals("1"))
                               {
                                   Encryptor enc = new Encryptor(getKeyBytes(key), main.getCode());
                                   //Encryptor enc = new Encryptor(kluczByte, getCode());
                                   s = enc.encrypt(s);
                               }
        			t.WriteCSharpString(main.socket.getOutputStream(),s);	 //wysy�anie pro�by o zmienna
       			
					}catch (Exception e) {
						showContext(e.getMessage());
						// TODO: handle exceptio
					}
				}
			});
			
			
			
			btnSet.setOnClickListener(new OnClickListener() {
				
				public void onClick(View v) {
					
					Translate t = new Translate();
					try
					{
					
					if(!etxtValue.getText().toString().trim().equals(""))
					{
						String nazwazmiennej = i.getName();
            			String value = etxtValue.getText().toString();
            			String s = main.MyAddress+":SET:"+nazwazmiennej+","+value;  

                                   String szyfr = main.preferences.getString("SZYFR_TCP", "");
                               if(!szyfr.equals("1"))
                               {
                                   Encryptor enc = new Encryptor(getKeyBytes(key), main.getCode());
                                   //Encryptor enc = new Encryptor(kluczByte, getCode());
                                   s = enc.encrypt(s);
                               }
                                
            			t.WriteCSharpString(main.socket.getOutputStream(),s);	
            			showContext("Próba zmiany danych");
					}else
					{
						showContext("Musisz podać wartość");
					}
					
					}catch (Exception e) {
						showContext(e.getMessage());
						// TODO: handle exception
					}
				}
			});

	}
	
	private void TimerMethod()
	{
		
		String s = main.MyAddress+":GET:"+nazwazmiennej;
		
		try {
                    
                       String szyfr = main.preferences.getString("SZYFR_TCP", "");
                               if(!szyfr.equals("1"))
                               {
                                   Encryptor enc = new Encryptor(getKeyBytes(key), main.getCode());
                                   //Encryptor enc = new Encryptor(kluczByte, getCode());
                                   s = enc.encrypt(s);
                               }
			t.WriteCSharpString(main.socket.getOutputStream(),s);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}	 //wysy�anie pro�by o zmienna
		catch(Exception ex)
                {
                    
                }
		//This method is called directly by the timer
		//and runs in the same thread as the timer.

		//We call the method that will work with the UI
		//through the runOnUiThread method.
		this.runOnUiThread(Timer_Tick);
	}

	private Runnable Timer_Tick = new Runnable() {
		public void run() {
			
				//showContext(nazwazmiennej);
				
		//This method runs in the same thread as the UI.    	       

		//Do something to the UI thread here

		}
	};
}


