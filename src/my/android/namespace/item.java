package my.android.namespace;

import java.io.Serializable;

/**
 * Klasa odpowiedzialna za przechowanie informacji o własnościach konkretnej zmiennej.
 * @author Daniel Marczydło
 */
@SuppressWarnings("serial") //with this annotation we are going to hide compiler warning
public class item implements Serializable {

	private int nr;
	private String name;
	private String type;
	private String address;
	private String description;

        /**
         * Akcesor ustawijący numer
         * @param newNr
         */
        public void setNr(int newNr) {
		this.nr = newNr;
	}

        /**
         * Akcesor ustawiający Nazwe
         * @param newName
         */
        public void setName(String newName) {
		this.name = newName;
	}

        /**
         * Akcesor ustawiający typ
         * @param newType
         */
        public void setType(String newType) {
		this.type = newType;
	}

        /**
         * Akcesor ustawiąjący adres
         * @param newAddress
         */
        public void setAddress(String newAddress) {
		this.address = newAddress;
	}

        /**
         * Akcesor ustawiąjący opis
         * @param newDescription
         */
        public void setDescription(String newDescription) {
		this.description = newDescription;
	}

        /**
         * Akcesor zwracający numer
         * @return numer zmiennej
         */
        public int getNr() {
		return this.nr;
	}

        /**
         * Akcesor zwracający nazwę zmiennej
         * @return nazwa zmiennej
         */
        public String getName() {
		return this.name;
	}

        /**
         * Akcesor zwracający adres zmiennej
         * @return adres zmiennej
         */
        public String getAddress() {
		return this.address;
	}

        /**
         * Akcesor zwracający typ zmiennej
         * @return typ zmiennej
         */
        public String getType() {
		return this.type;
	}

        /**
         * Akcesor zwracający opis zmiennej
         * @return opis zmiennej
         */
        public String getDescription() {
		return this.description;
	}

}
