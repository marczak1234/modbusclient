package my.android.namespace;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Enumeration;
import java.net.NetworkInterface;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;

import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.text.format.Formatter;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 
 * @author Daniel Marczydło
 */
public class main extends Activity {
	
	private Button btnLogin;
	private Button btnLogout;
        /**
         * zmienna przechowująca ciąg znaków.
         */
        public static String key = "tojestkluczwlasnie";
       //public byte[] kluczByte =  {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
	private Button VPN;
        /**
         * zmienna przechowująca aktualny adres urządzenia.
         */
        public static String MyAddress;
        /**
         * zmienna sprawdzająca, czy nawiązano połączenie
         */
        public static boolean running = false;
        /**
         * zmienna opowiedzialna za dostęp do menu.
         */
        public static SharedPreferences preferences;
	
        /**
         * zmienna sprawdzająca, czy wybrano szyfrowanie.
         */
        public boolean szyfrowanie = false;
        /**
         * Obiekt gniazda.
         */
        public static Socket socket = null;
	
	Translate t = new Translate();
	
        /**
         * Metoda wywoływana podczas tworzenia menu.
         * @param menu
         * @return
         */
        public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu,menu);
		return true;
		}

        /**
         * Metoda wywoływana podczas wyboru elementu z menu.
         * @param item
         * @return
         */
        @Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.pomoc:
			break;
		case R.id.ustawienia: 
			{
				Intent settingsActivity = new Intent(getBaseContext(),
                        settings.class);
                startActivity(settingsActivity);
				
			}break;
		case R.id.opogramie: 
		{
			PackageInfo pInfo=null;
			try {
				pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			} catch (NameNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			String version = pInfo.versionName;
			String textT = "Autor : Marczak \nWersja programu : "+ version + "\nOpis : Mobilny klient, którego zadaniem jest zmiana zmiennych procesowych oraz odczytywanie ich wartości (także cyklicznie). \nMożliwości programu : \n- odczyt wartości zmiennych procesowych MODBUS \n- zmiana wartości zmiennych procesowych \n- dostęp do zmiennych sieciowych oraz z poza sieci (VPN).  " ;
			final Dialog dialog = new Dialog(this);
			dialog.setContentView(R.layout.dualogabout);
			dialog.setTitle("Informacje o programie");
			dialog.setCancelable(true);
			TextView text = (TextView) dialog.findViewById(R.id.TextView01);
			text.setText(textT);
			
			Button button = (Button) dialog.findViewById(R.id.Button01);
			
			  dialog.show();
			
            button.setOnClickListener(new OnClickListener() {
           
                public void onClick(View v) {
                  
                	dialog.cancel();
                }
            });
			
          
			 
		
		}break;
		
		//default:	
		
		
		
		
		}
		// TODO Auto-generated method stub
		return super.onOptionsItemSelected(item);
				
	}

        /**
         * Metoda zwracająca bajt określający wybrane szyfrowanie.
         * @return
         */
        public static byte getCode()
        {
            byte code = 0;
            String szyfrr = preferences.getString("SZYFR_TCP", "");

            
            if(szyfrr.equals("2")){ code = 1; }
            else if(szyfrr.equals("5")){ code = 2;  }
            else if(szyfrr.equals("10")){ code = 3; }
            else if(szyfrr.equals("60")){ code = 4; }
          
            return code;
        }     
        
    /**
     * Metoda zwracająca klucz w postaci tablicy bajtów.
     * @param key
     * @return
     * @throws UnsupportedEncodingException
     */
    public static byte[] getKeyBytes(String key) throws UnsupportedEncodingException{
        byte[] keyBytes= new byte[16];
        byte[] parameterKeyBytes= key.getBytes("UTF-8");
        System.arraycopy(parameterKeyBytes, 0, keyBytes, 0, Math.min(parameterKeyBytes.length, keyBytes.length));
      return keyBytes;
    }


   /**
    * Metoda zamykająca dostępne gniazda sieciowe.
    */
   public static void socketClose()
	{
		try {
			
			socket.close();
			socket = null;
			
		} catch (IOException e) {
			//showContext(e.getMessage());
			e.printStackTrace();
		}
		
		
	}
	
	
   /**
    * Metoda pozwalająca na wyświetlenie wiadomości kontekstowej.
    * @param message
    */
   public void showContext(String message)
	{
		CharSequence text = (CharSequence)message;
        Context context = getApplicationContext();
        int duration = Toast.LENGTH_SHORT;
         
        Toast toast = Toast.makeText(context,text,duration);
        toast.show();    
	}
	
        
        /**
         * Metoda pobierająca lokalny adres IP
         * @return
         */
        public String getLocalIpAddress()
    {
      try {
          for (Enumeration<NetworkInterface> en = 
             NetworkInterface.getNetworkInterfaces();  
              en.hasMoreElements();) {
              NetworkInterface intf = en.nextElement();
              for (Enumeration<InetAddress> enumIpAddr =
              intf.getInetAddresses(); 
               enumIpAddr.hasMoreElements();) {
                  InetAddress inetAddress = enumIpAddr.nextElement();
                  if (!inetAddress.isLoopbackAddress()) {
                      return inetAddress.getHostAddress().toString();
                  }
              }
          }
      } catch (Exception ex) {
          //Log.e("IP Address", ex.toString());
      }
      return null;
  }
        /**
         * Metoda do obsługi kodowania MD5
         * @param input
         * @return
         */
        public static String getMD5(String input) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(input.getBytes());
            BigInteger number = new BigInteger(1, messageDigest);
            String hashtext = number.toString(16);
            // Now we need to zero pad it if you actually want the full 32 chars.
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        }
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
      }
	
	private void LogIn(String IP, String Port)
	{	
            
		try
		{
                    
			
			InetAddress serverAddr = InetAddress.getByName(IP);
		int PortI = Integer.parseInt(Port);
	        socket = new Socket(serverAddr, PortI);	
	        //showContext("aaa");
                //String ip = getLocalIpAddress();
               
                //ip = ip.substring(1,ip.length());
                //MyAddress = ip;
                //showContext(ip);
                String has = this.generateHas();
	       
                
                Encryptor enc = null; 
	       String s = MyAddress+":HI:"+has;
               //showContext(s);
               String szyfr = preferences.getString("SZYFR_TCP", "");
                               if(!szyfr.equals("1"))
                               {
                                  
                                   
                                  // showContext(szyfr);
                                 enc = new Encryptor(getKeyBytes(key),getCode());
                                   //enc = new Encryptor(kluczByte,getCode());
                                   s = enc.encrypt(s);
                                 
                               }
               
	        t.WriteCSharpString(socket.getOutputStream(),s);
                
	        String odp  =  "";
 
			running = true;	
			if(running)
			{
				btnLogin.setEnabled(false);
				
			}
			
        	
        		try
        		{
        			odp = t.ReadCSharpString(socket.getInputStream());
                                
                                String odpowiedz = null;
                                odpowiedz = odp;
                              
                                //szyfr = preferences.getString("SZYFR_TCP", "");
                                if(!szyfr.equals("1"))
                               {
                                   odpowiedz = enc.decrypt(odp);
                                 
                               }
                               
                                
        			if(odpowiedz != null)
        			{
                                   
        				 String HI = "HI";
                                          
        				if(odpowiedz.equals(HI))
        				{	
                                             this.showContext("Połączono z serwerem");
        					//zmiana ekranu
        					startActivity(new Intent("my.android.namespace.VARIABLE"));
        					
        				}else showContext(odpowiedz);
        			}
        		}
        		catch(Exception ex)
        		{
        			
        		}
      
    }
		catch (Exception e)
	    {
			this.showContext("błąd" + e.getMessage());
	        e.printStackTrace();
	        btnLogin.setEnabled(true);
			running = false;
	        this.showContext(e.getMessage());	
	    }
			if(!running)
			{
				try 
				{
					
					socket.close();
					socket = null;
					
					btnLogin.setEnabled(true);
		        					} 
				catch (IOException e) 
				{
					//e.printStackTrace();
				}	
		}
	}
	
	
        /**
         * 
         */
        @Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		
		
	}

        /**
         * 
         * @param savedInstanceState
         */
        protected void onResume(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
               
        }
        
        /**
         * 
         * @return
         */
        public String generateHas()
        {
            Date date = Calendar.getInstance().getTime();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String a =  sdf.format(date).toString();
                a = getMD5(a);
            return a;
        }
        
        /**
         * 
         * @param savedInstanceState
         */
        @Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		
             
		preferences = getSharedPreferences("myPreferences", Activity.MODE_PRIVATE);
		
		try
		{
                    
			WifiManager wifimanagerr = (WifiManager)getBaseContext().getSystemService(Context.WIFI_SERVICE);
			if(wifimanagerr.isWifiEnabled())
			{
					
				MyAddress =  Formatter.formatIpAddress(wifimanagerr.getConnectionInfo().getIpAddress());
			
			this.showContext(MyAddress);
			}else 
				{
					showContext("Włącz WIFI");
				}
				
			
		
		}catch (Exception e) {
			showContext("Wyjątek " + e.getMessage());
		}
        
		btnLogin = (Button)findViewById(R.id.btnPolacz);
		btnLogout = (Button)findViewById(R.id.btnRozlacz);
		VPN = (Button)findViewById(R.id.btnVPN);
		
		btnLogin.setEnabled(true);
	        btnLogout.setEnabled(false);
		
		VPN.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
			Intent intent = new Intent("my.android.namespace.VPN");
			startActivity(intent);
			}
		});
		
		
		btnLogout.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				if(socket != null)
				{
				try {
			              String s = MyAddress+":BYE:pusty:";
                                      String szyfr = preferences.getString("SZYFR_TCP", "");
                               if(!szyfr.equals("1"))
                               {
                                   Encryptor enc = new Encryptor(getKeyBytes(key), getCode());
                                  // Encryptor enc = new Encryptor(kluczByte, getCode());
                                   s = enc.encrypt(s);
                               }
                                      
                                      t.WriteCSharpString(socket.getOutputStream(),s);
				
                                      socket.close();
                                      btnLogin.setEnabled(true);
				      btnLogout.setEnabled(false);
                                    } catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				socket = null;
				
				
				
			}
			else
			{
				showContext("Musisz się najpierw zalogować");
			}}
		});
		
		
		btnLogin.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				
				try
				{
					
					String IP = preferences.getString("IP_SETTINGS", "");
				
					String PORT = preferences.getString("PORT_SETTINGS", "");
					
				
                                         
					LogIn(IP, PORT);
					
					if(socket.isConnected())
					{
						
						btnLogin.setEnabled(false);
						btnLogout.setEnabled(true);
					}
					else
					{
						
						btnLogin.setEnabled(true);
						btnLogout.setEnabled(false);
					}
	
				}catch (Exception e) {
					showContext(e.getMessage());
				}
				
			}
		});
		
	}
	
        /**
         * 
         */
        protected void onDestroy() {
	        super.onStop();
	        try
	        {
	        	if(socket != null)
	        	{
	        		String s = MyAddress+":BYE:pusty:";
                                String szyfr = preferences.getString("SZYFR_TCP", "");
                               if(!szyfr.equals("1"))
                               {
                                   Encryptor enc = new Encryptor(getKeyBytes(key), getCode());
                                  // Encryptor enc = new Encryptor(kluczByte, getCode());
                                   s = enc.encrypt(s);
                               }
                                
                                
	        		t.WriteCSharpString(socket.getOutputStream(),s);
	        		socket.close();
	        		socket = null;
	        	}
	        }catch (Exception e) {showContext(e.getMessage());}
	 }

	
}
