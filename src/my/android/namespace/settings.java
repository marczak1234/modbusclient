package my.android.namespace;



import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceScreen;
import android.util.Log;
/**
 * Klasa odpowiedzialna za zarządzanie ustawieniami apliakcji. Przechowuje wszystkie ustawienia.
 * @author Daniel Marczydło
 */
public class settings extends PreferenceActivity {
	
	
	private CheckBoxPreference ck;
	private ListPreference lsCk;
	private ListPreference themes;
        private ListPreference szyfry;
        /**
         * 
         */
        public static boolean CYKLICZNIE; 
	private EditTextPreference IP;
	private EditTextPreference PORT;

	
        /**
         * Metoda wywoływana podczas powrotu do menu.
         */
        @Override
	protected void onPause() {
		
		SharedPreferences mySettings = getSharedPreferences("myPreferences", Activity.MODE_PRIVATE);  
		SharedPreferences.Editor prefEditor = mySettings.edit();  
		prefEditor.putString("IP_SETTINGS",IP.getText().toString());  
		prefEditor.putString("PORT_SETTINGS", PORT.getText().toString());  
		prefEditor.putBoolean("CK_SETTINGS",ck.isChecked());
		prefEditor.putString("CK_VALUE_SETTINGS", lsCk.getValue());
                prefEditor.putString("SZYFR_TCP", szyfry.getValue());
                Log.e("Ustawienia", "Wartość = " + szyfry.getValue());
		prefEditor.commit();  
		// TODO Auto-generated method stub
		super.onPause();
	}


        /**
         * Metoda wywoływana podczas wejścia do widoku menu.
         * @param savedInstanceState
         */
        @Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.settings);
		
		ck = (CheckBoxPreference)findPreference("checkboxOdp");
		lsCk = (ListPreference)findPreference("listOdp");
		themes = (ListPreference)findPreference("listSk");
		IP = (EditTextPreference)findPreference("LogInIP");
		PORT = (EditTextPreference)findPreference("LogInPort");
		szyfry = (ListPreference)findPreference("listSzyfr");
		
		Intent intent = getIntent();
		
		boolean ipport = intent.getBooleanExtra("IPPORT", false);
		PreferenceCategory cat = (PreferenceCategory)findPreference("ustsiec");
		if(ipport)
		{
			cat.setEnabled(false);
		}
		else 
		{
			cat.setEnabled(true);
		}
		
		
		CYKLICZNIE = ck.isChecked();
		
		if(CYKLICZNIE)
		{
			lsCk.setEnabled(true);
		}else lsCk.setEnabled(false);
	
	
	ck.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {            
        public boolean onPreferenceChange(Preference preference, Object newValue) {
        	 
        	CYKLICZNIE =  Boolean.valueOf(newValue.toString());
        	if(CYKLICZNIE)
        	{
        		lsCk.setEnabled(true);
        	}else lsCk.setEnabled(false);
        	
           // Log.d("MyApp", "Pref " + preference.getKey() + " changed to " + newValue.toString());       
            return true;
        }
    });
	}
}
